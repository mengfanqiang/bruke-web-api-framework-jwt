﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class UserService
    {
        public User Login(string loginAcount, string loginPwd)
        {
            var db = new DBEntities();
            var user = db.User.FirstOrDefault(w => w.loginName == loginAcount && w.loginPwd == loginPwd);
            return user;
        }
    }
}
