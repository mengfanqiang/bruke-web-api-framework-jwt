﻿using Business;
using Common;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class UserController : ApiController
    {
        [HttpGet]
        public Result Login(string loginAcount, string loginPwd)
        {
            var result = new Result(CustomCodeEnum.Failed);
            var userService = new UserService();
            var user= userService.Login(loginAcount, loginPwd);
            if (user==null)
            {
                result.Message = "帐号或密码错误";
                return result;
            }
            var jwtModel=new JWTModel();
            jwtModel.iatTime = DateTime.Now;
            jwtModel.expTime = DateTime.Now.AddDays(1);
            jwtModel.userId = user.id;
            jwtModel.customData = new { id=user.id,name=user.loginName};
            var token= JWTHelper.Generate(jwtModel);

            result.SetCode(CustomCodeEnum.Success);
            result.Data = token;
            result.Message = "登录成功";
            return result;
        }
    }
}
