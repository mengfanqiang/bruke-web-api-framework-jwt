﻿using Common;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace WebApi.Controllers
{
    /// <summary>
    /// token认证
    /// </summary>
    [UsersAuthorize]
    public class HomeController : ApiController
    {
        /// <summary>
        /// Get一下试试
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Result GetHome()
        {
            var data = new { name = "我的名字叫bruke", id = 1 };
            return new Result() { Data = data };
        }

        /// <summary>
        /// webapi 不查数据库，看你token知道你是谁
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Result KownWho()
        {
            var user = JWTHelper.GetUser();
            return new Result() { Data= user };
        }
    }
}
