﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class JWTModel
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public int userId { get; set; }
        /// <summary>
        /// 浏览器指纹
        /// </summary>
        public string fingerprint { get; set; }
        /// <summary>
        /// ip地址
        /// </summary>
        public string ip { get; set; }
        /// <summary>
        /// 颁发日期
        /// </summary>
        public DateTime iatTime { get; set; }
        /// <summary>
        /// 过期日期
        /// </summary>
        public DateTime expTime { get; set; }
        /// <summary>
        /// 自定义数据
        /// </summary>
        public object customData { get; set; }
    }
}
