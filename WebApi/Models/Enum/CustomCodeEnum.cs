﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public enum CustomCodeEnum
    {
        /// <summary>
        /// 成功
        /// </summary>
        [Description("Success")]
        Success = 10000,

        /// <summary>
        /// 失败
        /// </summary>
        [Description("Fail")]
        Failed = 10001,

        /// <summary>
        /// 身份过期
        /// </summary>
        [Description("Identity expired")]
        Expired = 20001
    }

}
