﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 统一api返回模型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Result<T> : Result
    {
        public new T Data { get; set; }
    }

    public class Result
    {
        public Result(CustomCodeEnum customCodeEnum = CustomCodeEnum.Success)
        {
            SetCode(customCodeEnum);
        }

        public CustomCodeEnum Code { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }

        public void SetCode(CustomCodeEnum errorCodeEnum)
        {
            Code = errorCodeEnum;
            Message = errorCodeEnum.GetRemark();
        }
    }
}