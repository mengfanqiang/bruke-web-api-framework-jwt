# BrukeWebApiFrameworkJWT

#### 介绍
WebApi+.net Framework 4.7 + JWT 无状态登录。
1.web 2.webapi 3.common 4.business 5.models 6.dal。EF6+webapi+前后端分离+JWT

#### 软件架构
本项目主要是给新手，了解webapi+JWT 是如何搭建，和运行原理。
采用的是前后端分离：webapi【WebApi】+前端网站【website】。
前提是安装好sqlserver。设置好账号密码。
完全可以用于生产环境。支持分布式和拓展。

#### 使用说明
1.  删除【文件夹packages】  这个文件夹是保存nuget下载的dll。删除是为了你可以还原nuget,自己还原就不会奇奇怪怪的问题。
    （比如，看我项目引用了EF6,然后右键【引用】管理nuget，发现下不了EF，也卸载不了EF等等。）所以删除packages文件夹就对了。

2.  右键【解决方案（.sln）】,还原Neget包。（你会发现多了一个packages文件夹,就是还原成功了）

3.  【项目DAL】 我放了一个DB.sql。你去sqlserver建一个数据库。执行这个sql就可以了。数据库名称：WebApiDB，里面有一个表：[User]

4.  【项目DAL】 链接数据库。在App.Config  的<connectionStrings>节点，修改成你自己的账号密码。（用于更新数据库表到代码用的）
    （项目DAL 里面有一个DbModel.edmx。这个是数据库映像。后续在数据库修改表结构，可以打开这个东西，【从数据库更新模型】）

5.  【项目WebApi】 链接数据库。在 Web.config 的<connectionStrings>节点，修改成你自己的账号密码。(用于正常代码业务访问数据库）

6.  到此，项目可以说配置完了。由于要看到项目效果，所以前后端一起启动，请根据下面设置启动。

7.  右键解决方案（.sln）,【设置启动项目】--[多个启动项目]，把webapi,Website这2个设置为启动。

8.  【F5或启动】即可

#### 编码关键词
后端：
要身份访问：[UsersAuthorizeAttribute]
匿名访问：[NotAuthorizeAttribute]
知道请求者是谁：JWTHelper.GetUser()；
请求者：JWTModel
登录代码有：jwtModel.customData= new { id=user.id,name=user.loginName},请自定义一个模型。敏感信息不要放入token.

前端：
浏览器缓存 localStorage.setItem("UserToken", result.Data);
浏览器缓存 localStorage.getItem("UserToken");
前端base64解码，得到用户信息
var jwtDataWords = CryptoJS.enc.Base64.parse(payload);
var jwtDataStr = jwtDataWords.toString(CryptoJS.enc.Utf8);
var jwtData = JSON.parse(jwtDataStr);
401 身份失效

#### 总结
以上一句话：无状态。
在知道用户是谁，基本信息时，不用session,cookie,redis,数据库等。就一个token就可以知道了。

#### 参与贡献
CaptainBruke
微信号：Bruke123
有问题可以gitee上留言或微信问我。

